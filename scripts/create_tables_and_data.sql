-- CREATE TABLES

CREATE SCHEMA IF NOT EXISTS import;

CREATE TABLE IF NOT EXISTS import.car_brands (
    id SERIAL PRIMARY KEY,
    brand text NOT NULL
);

CREATE TABLE IF NOT EXISTS import.car_models (
    id SERIAL PRIMARY KEY,
    model text NOT NULL,
    brand_id integer REFERENCES import.car_brands(id)
);

CREATE TABLE IF NOT EXISTS import.categories (
    id SERIAL PRIMARY KEY,
    category text NOT NULL
);

CREATE TABLE IF NOT EXISTS import.ads (
    id SERIAL   PRIMARY KEY,
    title text  NOT NULL,
    content text    NOT NULL,
    category integer REFERENCES import.categories(id),
    car_model integer REFERENCES import.car_models(id),
    car_brand integer REFERENCES import.car_brands(id),
    check((category=3 AND car_model IS NOT NULL AND car_brand IS NOT NULL) OR (category != 3))
);

-- FILL WITH DATA

INSERT INTO import.categories (category)
VALUES ('jobs'),
       ('estate'),
       ('cars');

INSERT INTO import.car_brands (brand)
VALUES ('Audi'),
       ('BMW'),
       ('Citroën');

INSERT INTO import.car_models (model, brand_id)
VALUES ('Cabriolet', 1),
    ('Q2', 1),
    ('Q3', 1),
    ('Q5', 1),
    ('Q7', 1),
    ('Q8', 1),
    ('R8', 1),
    ('Rs3', 1),
    ('Rs4', 1),
    ('Rs5', 1),
    ('Rs7', 1),
    ('S3', 1),
    ('S4', 1),
    ('S4 Avant', 1),
    ('S4 Cabriolet', 1),
    ('S5', 1),
    ('S7', 1),
    ('S8', 1),
    ('SQ5', 1),
    ('SQ7', 1),
    ('Tt', 1),
    ('Tts', 1),
    ('V8', 1),
    ('M3', 2),
    ('M4', 2),
    ('M5', 2),
    ('M535', 2),
    ('M6', 2),
    ('M635', 2),
    ('Serie 1', 2),
    ('Serie 2', 2),
    ('Serie 3', 2),
    ('Serie 4', 2),
    ('Serie 5', 2),
    ('Serie 6', 2),
    ('Serie 7', 2),
    ('Serie 8', 2),
    ('C1', 3),
    ('C15', 3),
    ('C2', 3),
    ('C25', 3),
    ('C25D', 3),
    ('C25E', 3),
    ('C25TD', 3),
    ('C3', 3),
    ('C3 Aircross', 3),
    ('C3 Picasso', 3),
    ('C4 Aircross', 3),
    ('C4 Picasso', 3),
    ('C5', 3),
    ('C6', 3),
    ('C8', 3),
    ('Ds3', 3),
    ('Ds4', 3),
    ('Ds5', 3);

INSERT INTO import.ads (title, content, category, car_model, car_brand)
VALUES ('À vendre appartement à Nantes', 'Superbe volume au moins 150m2, sur l ile de Nantes', 2, NULL, NULL),
       ('Recherche développeur fullstack sur Paris', 'Au moins 5 ans d expérience', 1, NULL, NULL),
       ('Audi TT tout confort', 'Véhicule de fonction, entretien à jour, moins de 100000km', 3, 21, 1),
       ('Citroën C1 de mémé', 'Peu utilisé, moins de 50000km, travaux à prévoir', 3, 38, 3);