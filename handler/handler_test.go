package handler

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/brieuc.urbain/import-ads-api/db"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var (
	testSearchJSONRequest  = `{"request":"Audi tt"}`
	testSearchJSONResponse = `[{"id":3,"title":"Audi TT tout confort","content":"Véhicule de fonction, entretien à jour, moins de 100000km","category":"cars","carModel":"Tt","carBrand":"Audi"}]
`
	testPushJSONRequest   = `[{"title": "ad title","content": "ad content","category": 3,"carModel": 7,"carBrand": 1}]`
	testUpdateJSONRequest = `[{"id": 1,"updates": {"title": "en fait c'est une Audi Q5x","content": "c'est une Q5x"}}]`
	testDeleteJSONRequest = `{"ids": [2]}`
)

type PushJSONResponse struct {
	Title    string `json:"title"`
	Content  string `json:"content"`
	Category string `json:"category"`
	Model    string `json:"carModel"`
	Brand    string `json:"carBrand"`
}

func TestSearchAd(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/api/v1/ads/search", strings.NewReader(testSearchJSONRequest))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	database, err := db.Initialize(
		"postgres", "password", "ads-db", "postgres-db:5432")
	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}

	defer func(database db.Database) {
		err := database.Conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(database)

	h := &Handler{Db: database}

	// Assertions
	if assert.NoError(t, h.Search(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, testSearchJSONResponse, rec.Body.String())
	}
}

func TestPushAd(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/api/v1/ads/push", strings.NewReader(testPushJSONRequest))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	database, err := db.Initialize(
		"postgres", "password", "ads-db", "postgres-db:5432")
	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}

	defer func(database db.Database) {
		err := database.Conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(database)

	h := &Handler{Db: database}

	// Assertions
	if assert.NoError(t, h.Push(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		request := PushJSONResponse{}
		json.Unmarshal([]byte(testPushJSONRequest), &request)
		response := PushJSONResponse{}
		res := rec.Body.String()
		json.Unmarshal([]byte(res), &response)

		assert.Equal(t, request, response)
	}
}

func TestUpdateAd(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/api/v1/ads/update", strings.NewReader(testUpdateJSONRequest))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	database, err := db.Initialize(
		"postgres", "password", "ads-db", "postgres-db:5432")
	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}

	defer func(database db.Database) {
		err := database.Conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(database)

	h := &Handler{Db: database}

	// Assertions
	if assert.NoError(t, h.Update(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		res := rec.Body.String()
		log.Println(res)
	}
}

func TestDeleteAd(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/api/v1/ads/update", strings.NewReader(testDeleteJSONRequest))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	database, err := db.Initialize(
		"postgres", "password", "ads-db", "postgres-db:5432")
	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}

	defer func(database db.Database) {
		err := database.Conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(database)

	h := &Handler{Db: database}

	// Assertions
	if assert.NoError(t, h.Delete(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		res := rec.Body.String()
		log.Println(res)
	}
}
