package handler

import (
	"encoding/json"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"gitlab.com/brieuc.urbain/import-ads-api/db"
	"gitlab.com/brieuc.urbain/import-ads-api/model"
	"gitlab.com/brieuc.urbain/import-ads-api/utils"
	"log"
	"net/http"
)

type SearchRequest struct {
	Request string `json:"request" form:"request" query:"request"`
}

type PushRequest struct {
	Title    string `json:"title" validate:"required"`
	Content  string `json:"content" validate:"required"`
	Category int    `json:"category"`
	CarModel int    `json:"carModel"`
	CarBrand int    `json:"carBrand"`
}

type DeleteRequest struct {
	Ids []int `json:"ids" form:"ids" query:"ids" validate:"required"`
}

type UpdateRequest struct {
	Id      int      `json:"id" form:"id" query:"id" validate:"required"`
	Updates model.Ad `json:"updates" validate:"required"`
}

func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}

type Handler struct {
	Db db.Database
}

func (h *Handler) Search(c echo.Context) error {
	// parsing request payload
	req := new(SearchRequest)
	if err := c.Bind(req); err != nil {
		log.Printf("Error while parsing JSON payload: %s", err)
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	log.Printf("Entering /ads/search POST, user request string: '%s'", req.Request)

	// find best matching car model based on user request
	bestMatch, err := findBestMatchInRequest(h.Db, req.Request)
	if err != nil {
		log.Printf("Could not find best match for user query: %s %s\n", req.Request, err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}
	//log.Printf("Best match, %s", prettyPrint(bestMatch))

	// get all ads with matching car model in DB
	ads, err := db.GetAllAdsWithCarModel(h.Db, []int{bestMatch.TestValue.RefId}, "car_model")
	if err != nil {
		log.Printf("Error cannot get ads: %s\n", err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	if len(ads) > 0 {
		log.Printf("Response for /ads/search POST, %s", prettyPrint(ads))
		return c.JSON(http.StatusOK, ads)
	}
	return c.NoContent(http.StatusNoContent)
}

func (h *Handler) Push(c echo.Context) error {
	// parsing request payload
	var req []model.Ad
	if err := c.Bind(&req); err != nil {
		log.Printf("Error while parsing JSON payload: %s", err)
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	log.Printf("Entering /ads/push POST, user request string: '%s'", prettyPrint(req))

	validate := validator.New()

	// get car Model data from DB
	carModelDetails, err := db.GetAllCarModelAndBrand(h.Db)
	if err != nil {
		log.Printf("Error cannot get car models: %s\n", err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	var enrichedAds []model.Ad
	// loop over ads in request to find matching car models and brands
	for _, ad := range req {
		// validate mandatory properties title and content
		err := validate.Struct(ad)
		if err != nil {
			return c.JSON(http.StatusBadRequest, "Missing mandatory property in body")
		}

		// if a car model is supplied and car brand is missing, find matching brand and enrich
		if ad.CarModel != 0 {
			ad.Category = 3
			if ad.CarBrand == 0 {
				for i := range carModelDetails {
					if carModelDetails[i].Id == ad.CarModel {
						ad.CarBrand = carModelDetails[i].BrandID
						break
					}
				}
			}
		} else {
			// if no model is supplied, concatenate title and description and try to match
			// individual string fields to available car models and enrich if score is sufficient
			matchStr := fmt.Sprintf("%s %s", ad.Title, ad.Content)

			// find best matching car model based on user request
			bestMatch, err := findBestMatchInRequest(h.Db, matchStr)
			if err != nil {
				log.Printf("Could not find best match for user query: %s %s\n", matchStr, err)
				return echo.NewHTTPError(http.StatusInternalServerError)
			}

			// only enrich car type add with matching model is score is sufficient
			if bestMatch.Score > 0.6 {
				ad.Category = 3

				// find matching entry in carModelDetails to enrich new ad
				for i := range carModelDetails {
					if carModelDetails[i].Id == bestMatch.TestValue.RefId {
						ad.CarModel = carModelDetails[i].Id
						ad.CarBrand = carModelDetails[i].BrandID
						break
					}
				}
			}

		}
		// in any case, always append ad (enriched or not depending on case)
		enrichedAds = append(enrichedAds, ad)
	}

	// insertInDB
	inserts, err := db.InsertAdsInDB(h.Db, enrichedAds)
	if err != nil {
		log.Printf("Could not insert new ads in DB: '%s'", err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	log.Printf("Response for /ads/push POST, %s", prettyPrint(inserts))
	return c.JSON(http.StatusOK, inserts)
}

func (h *Handler) Delete(c echo.Context) error {
	// parsing request payload
	req := new(DeleteRequest)
	if err := c.Bind(req); err != nil {
		log.Printf("Error while parsing JSON payload: %s", err)
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	log.Printf("Entering /ads/delete DELETE, user request string: '%s'", prettyPrint(req))

	validate := validator.New()
	err := validate.Struct(req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Missing mandatory property in body")
	}

	count, err := db.DeleteAdsFromDBWithID(h.Db, req.Ids)
	if err != nil {
		log.Printf("Could not delete ads from DB: '%s'", err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	log.Printf("Response for /ads/delete DELETE: deleted %d ads", count)
	return c.JSON(http.StatusOK, fmt.Sprintf("Deleted %d ads", count))
}

func (h *Handler) Update(c echo.Context) error {
	// parsing request payload
	var req []UpdateRequest
	if err := c.Bind(&req); err != nil {
		log.Printf("Error while parsing JSON payload: %s", err)
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	log.Printf("Entering /ads/update PATCH, user request string: '%s'", prettyPrint(req))

	// validate req body
	validate := validator.New()

	// get car Model data from DB
	carModelDetails, err := db.GetAllCarModelAndBrand(h.Db)
	if err != nil {
		log.Printf("Error cannot get car models: %s\n", err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	var enrichedAds []model.Ad
	// loop over ads in request to find matching car models and brands
	for _, ad := range req {
		adInDB, err := db.GetAdWithID(h.Db, ad.Id)
		if err != nil {
			log.Printf("Could not get ad in DB: '%s'", err)
			return echo.NewHTTPError(http.StatusInternalServerError)
		}
		updatedAd := model.Ad{Id: adInDB.Id, Title: adInDB.Title, Content: adInDB.Content, Category: adInDB.Category, CarModel: adInDB.CarModel, CarBrand: adInDB.CarBrand}
		var matchStr string

		updatedAd.Id = ad.Id
		if ad.Updates.Title != "" {
			matchStr = fmt.Sprintf("%s %s", matchStr, ad.Updates.Title)
			updatedAd.Title = ad.Updates.Title
		}
		if ad.Updates.Content != "" {
			matchStr = fmt.Sprintf("%s %s", matchStr, ad.Updates.Content)
			updatedAd.Content = ad.Updates.Content
		}
		if ad.Updates.CarBrand != 0 {
			updatedAd.CarBrand = ad.Updates.CarBrand
		}

		// validate mandatory properties
		err = validate.Struct(ad)
		if err != nil {
			return c.JSON(http.StatusBadRequest, "Missing mandatory property in body")
		}

		// if a car model is supplied and car brand is missing, find matching brand and enrich
		if ad.Updates.CarModel != 0 {
			updatedAd.Category = 3
			for i := range carModelDetails {
				if carModelDetails[i].Id == ad.Updates.CarModel {
					updatedAd.CarBrand = carModelDetails[i].BrandID
					break
				}
			}
		} else if len(matchStr) > 0 {
			// if at least title or content have changed, try to match the concatenation
			// individual string fields to available car models and enrich if score is sufficient

			// find best matching car model based on user request
			bestMatch, err := findBestMatchInRequest(h.Db, matchStr)
			if err != nil {
				log.Printf("Could not find best match for user query: %s %s\n", matchStr, err)
				return echo.NewHTTPError(http.StatusInternalServerError)
			}

			// only enrich car type add with matching model is score is sufficient
			if bestMatch.Score > 0.6 {
				updatedAd.Category = 3

				// find matching entry in carModelDetails to enrich new ad
				for i := range carModelDetails {
					if carModelDetails[i].Id == bestMatch.TestValue.RefId {
						updatedAd.CarModel = carModelDetails[i].Id
						updatedAd.CarBrand = carModelDetails[i].BrandID
						break
					}
				}
			}

		}
		// in any case, always append ad (enriched or not depending on case)
		enrichedAds = append(enrichedAds, updatedAd)
	}

	// update DB ads
	count, err := db.UpdatesAdsWithID(h.Db, enrichedAds)
	if err != nil {
		log.Printf("Could not update ads in DB: '%s'", err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	log.Printf("Response for /ads/update PATCH: updated %d ads", count)
	return c.JSON(http.StatusOK, fmt.Sprintf("Updated %d ads", count))
}

func findBestMatchInRequest(database db.Database, reqStr string) (utils.Similarity, error) {
	// normalize request and split into words on whitespaces
	normalizedRequest, err := utils.GetNormalizedFields(reqStr)
	if err != nil {
		log.Printf("Could not normalize user request %s", err)
		return utils.Similarity{}, err
	}

	// get normalized car Model data from DB
	normCarModelData, err := getNormCarModelTestValues(database)
	if err != nil {
		log.Printf("Could not get normalized car model data from DB %s", err)
		return utils.Similarity{}, err
	}

	// test normalized request vs normalized car DB models to find best match
	bestMatch := utils.FindBestMatch(normCarModelData, normalizedRequest)

	return bestMatch, nil
}

func getNormCarModelTestValues(database db.Database) ([]utils.TestValue, error) {
	carModelDetails, err := db.GetAllCarModelAndBrand(database)
	if err != nil {
		log.Printf("Error cannot get car models: %s\n", err)
		return nil, err
	}
	carModelValues := make(map[int]string)
	for _, carModel := range carModelDetails {
		carModelValues[carModel.Id] = carModel.Model
	}

	carModelTestRef := utils.GenerateTestValues(carModelValues)
	err = utils.NormalizeTestValues(carModelTestRef)
	if err != nil {
		log.Printf("Error during normlalization of car models: %s\n", err)
		return nil, err
	}
	var carModelTestValues []utils.TestValue
	for _, val := range carModelTestRef {
		carModelTestValues = append(carModelTestValues, *val)
	}

	return carModelTestValues, nil
}
