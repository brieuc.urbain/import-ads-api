package db

import (
	"context"
	"github.com/go-pg/pg"
	"gitlab.com/brieuc.urbain/import-ads-api/model"
	"log"
)

type Database struct {
	Conn *pg.DB
}

type AdDB struct {
	tableName struct{} `sql:"import.ads"`
	model.Ad
}

type FullAdDB struct {
	tableName struct{} `sql:"import.ads"`
	model.FullAd
}

type CarModelDetails struct {
	tableName struct{} `sql:"import.car_models"`
	model.CarModel
}

func Initialize(username, password, database, addr string) (Database, error) {
	db := Database{}
	db.Conn = pg.Connect(&pg.Options{
		Addr:     addr,
		User:     username,
		Password: password,
		Database: database,
	})

	ctx := context.Background()
	_, err := db.Conn.ExecContext(ctx, "SELECT 1")
	if err != nil {
		panic(err)
	}
	log.Println("Database connection established")
	return db, nil
}

func GetAllAdsWithCarModel(database Database, matchingIds []int, matchingColumn string) ([]FullAdDB, error) {
	var fullAds []FullAdDB
	query := `SELECT t1.id, t1.title, t1.content, t2.category, t3.model, t4.brand FROM import.ads t1 INNER JOIN import.categories t2 ON t1.category = t2.id INNER JOIN import.car_models t3 ON t1.car_model = t3.id INNER JOIN import.car_brands t4 ON t1.car_brand = t4.id WHERE ? in (?)`
	_, err := database.Conn.Query(&fullAds, query, pg.F(matchingColumn), pg.In(matchingIds))
	if err != nil {
		log.Printf("Error cannot get ads: %s\n", err)
		return nil, err
	}

	return fullAds, nil
}

func GetAllCarModelAndBrand(database Database) ([]CarModelDetails, error) {
	var carModelDetails []CarModelDetails
	query := `SELECT t1.id, t1.model, t1.brand_id, t2.brand FROM import.car_models t1 INNER JOIN import.car_brands t2 ON t1.brand_id = t2.id`
	_, err := database.Conn.Query(&carModelDetails, query)
	if err != nil {
		log.Printf("Error cannot get car models: %s\n", err)
		return nil, err
	}

	return carModelDetails, nil
}

func InsertAdsInDB(database Database, ads []model.Ad) ([]AdDB, error) {
	var adsDB []AdDB
	for _, ad := range ads {
		adsDB = append(adsDB, AdDB{struct{}{}, ad})
	}
	err := database.Conn.Insert(&adsDB)

	return adsDB, err
}

func GetAdWithID(database Database, id int) (AdDB, error) {
	var ad AdDB
	err := database.Conn.Model(&ad).
		Where("id = ?", id).
		Select()
	if err != nil {
		log.Printf("Error cannot get ads: %s\n", err)
		return AdDB{}, err
	}

	return ad, nil
}

func UpdatesAdsWithID(database Database, ads []model.Ad) (int, error) {
	var adsDB []*AdDB
	for _, ad := range ads {
		adsDB = append(adsDB, &AdDB{struct{}{}, ad})
	}

	result, err := database.Conn.
		Model(&adsDB).
		WherePK().
		Update()
	if err == nil {
		count := result.RowsAffected()
		return count, nil
	}

	return 0, err
}

func DeleteAdsFromDBWithID(database Database, ids []int) (int, error) {
	var adsDB []AdDB
	result, err := database.Conn.
		Model(&adsDB).
		Where("id IN (?)", pg.In(ids)).
		Delete()
	if err == nil {
		count := result.RowsAffected()
		return count, nil
	}

	return 0, err
}
