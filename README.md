# import-ads-api

A simple API in Go with a PostgreSQL connector to manage advertisements

## Run the API

`Docker` and `docker-compose` are required to run this code.

Clone project (public) and `cd` into local folder.

Run the app and spin up the database with :
```
docker-compose up --build app postgres-db
```

You can then call the API on address `localhost:5000/api/v1/<route_name>` using your favourite tool (cURL, Postman, etc).

## Run integration and unit tests

In order to run the tests, start API (see previous step).


Open another terminal, `cd` into local folder and run a shell in the API container :
```
docker-compose exec app sh
```

Run the tests with coverage for each package :
```
go test -v -coverpkg=./... ./...
```

When done, leave container shell by typing `exit`

## Call the API

### Routes

#### SEARCH
```
POST host:port/api/v1/ads/search
```
with request body 
```json
{
  "request": "your request here"
}
```

using cURL:
```
curl -X POST "http://127.0.0.1:5000/api/v1/ads/search" -u "usr:password" -H "Content-Type: application/json" -d '{
  "request": "citroën c1x"
}'
```

#### PUSH
```
POST host:port/api/v1/ads/push
```
with request body
```json
[
  {
    "title": "ad title",
    "content": "ad content",
    "category": 3,
    "carModel": 7,
    "carBrand": 1
  }
]
```
Only `title` and `content` are mandatory

using cURL:
```
curl -X POST "http://127.0.0.1:5000/api/v1/ads/push" -u "usr:password" -H "Content-Type: application/json" -d '[
  {
    "title": "ad title",
    "content": "ad content",
    "category": 3,
    "carModel": 7,
    "carBrand": 1
  }
]'
```

#### DELETE
```
DELETE host:port/api/v1/ads/delete
```
with request body
```json
{
  "ids": [
    2
  ]
}
```
Specific ids of ads to be removed

using cURL:
```
curl -X DELETE "http://127.0.0.1:5000/api/v1/ads/delete" -u "usr:password" -H "Content-Type: application/json" -d '{
  "ids": [
    2
  ]
}'
```

#### UPDATE
```
PATCH host:port/api/v1/ads/update
```
with request body
```json
[
  {
    "id": 1,
    "updates": {
      "title": "en fait c'est une Audi Q5x",
      "content": "c'est une Q5x"
    }
  },
  {
    "id": 7,
    "updates": {
      "carModel": 3
    }
  }
]
```
If car model id is supplied, ad is enriched with matching car brand, if not the new content & title are used to extract possible car model value.

using cURL:
```
curl -X PATCH "http://127.0.0.1:5000/api/v1/ads/update" -u "usr:password" -H "Content-Type: application/json" -d '[{"id": 1,"updates": {"title": "en fait c est une Audi Q5x", "content": "c est une Q5x"}}]'

```

## Shutdown API

Press Ctrl+C to close running containers gracefully.

Shut down running containers with :
```
docker-compose down
```

