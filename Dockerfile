# syntax=docker/dockerfile:1
FROM golang:1.18.2-alpine as builder

ENV CGO_ENABLED=0

WORKDIR app/

# Download necessary Go modules
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./
COPY db/ ./db
COPY handler/ ./handler
COPY model/ ./model
COPY utils/ ./utils

RUN go build

CMD [ "./import-ads-api" ]
