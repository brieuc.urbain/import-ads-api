package model

type Ad struct {
	Id       int    `json:"id"`
	Title    string `json:"title"`
	Content  string `json:"content"`
	Category int    `json:"category"`
	CarModel int    `json:"carModel"`
	CarBrand int    `json:"carBrand"`
}

type FullAd struct {
	Id       int    `json:"id"`
	Title    string `json:"title"`
	Content  string `json:"content"`
	Category string `json:"category"`
	Model    string `json:"carModel"`
	Brand    string `json:"carBrand"`
}

type CarModel struct {
	Id      int    `json:"id"`
	Model   string `json:"model"`
	BrandID int    `pg:"rel:has-one,fk:id"`
	Brand   string
}
