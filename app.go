package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/brieuc.urbain/import-ads-api/db"
	"gitlab.com/brieuc.urbain/import-ads-api/handler"
	"log"
	"net/http"
)

var database db.Database

func redirect(c echo.Context) error {
	url := fmt.Sprintf("%s://%s/api/v1", c.Scheme(), c.Request().Host)
	return c.Redirect(301, url)
}

// @title Ads API
// @version 1.0
// @description Mock ads API

// @host localhost
// @schemes http
// @BasePath /api/v1
func main() {
	var err error
	database, err = db.Initialize(
		"postgres", "password", "ads-db", "postgres-db:5432")
	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}

	defer func(database db.Database) {
		err := database.Conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(database)

	users := map[string]string{
		"usr": "password",
	}

	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet},
	}))

	// redirect GET / to /api/vX
	e.GET("/", redirect)

	g := e.Group("/api/v1")
	g.Use(middleware.BasicAuthWithConfig(middleware.BasicAuthConfig{
		Validator: func(username, password string, c echo.Context) (bool, error) {
			for k, v := range users {
				if username == k && password == v {
					return true, nil
				}
			}
			return false, nil
		},
	}))

	// handler
	var h = handler.Handler{database}

	// Route
	g.POST("/ads/search", h.Search)
	g.POST("/ads/push", h.Push)
	g.DELETE("/ads/delete", h.Delete)
	g.PATCH("/ads/update", h.Update)

	// PORT Value
	port := ":5000"

	e.Logger.Fatal(e.Start(port))
}
