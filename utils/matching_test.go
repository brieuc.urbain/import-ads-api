package utils

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestNormalizeString(t *testing.T) {
	testValues := []string{"Citroën", "DS4", "éêè"}
	expected := []string{"citroen", "ds4", "eee"}

	for index, test := range testValues {
		t.Run(test, func(t *testing.T) {
			result, _ := NormalizeString(test)
			if !assert.Equal(t, result, expected[index]) {
				t.Fatalf("Error: want %v, got %v", expected[index], result)
			}
		})
	}
}

func TestGetNormalizedFields(t *testing.T) {
	testValue := "je cherche une Citroën DS4 Aircross"
	expected := []string{"je", "cherche", "une", "citroen", "ds4", "aircross"}

	result, _ := GetNormalizedFields(testValue)
	if !reflect.DeepEqual(expected, result) {
		t.Fatalf("Error: want %v, got %v", expected, result)
	}
}

func TestGenerateTestValues(t *testing.T) {
	testValue := map[int]string{1: "DS4", 2: "Citroën"}

	result := GenerateTestValues(testValue)

	for _, tv := range result {
		if !(reflect.TypeOf(*tv).String() == "utils.TestValue") {
			t.Fatalf("Error: response should be of type TestValue, not %v", *tv)
		}
	}
}

func TestNormalizeTestValues(t *testing.T) {
	expected := []string{"ds4", "citroen"}
	value1 := TestValue{"DS4", 1}
	value2 := TestValue{"Citroën", 2}
	testValue := []*TestValue{&value1, &value2}

	NormalizeTestValues(testValue)

	assert.Equal(t, expected[0], testValue[0].Value)
	assert.Equal(t, expected[1], testValue[1].Value)
}

func TestFindBestMatch(t *testing.T) {
	testValue := []TestValue{{"ds4", 1}, {"tt", 2}}

	result := FindBestMatch(testValue, []string{"ds3"})

	assert.Equal(t, result, Similarity{"ds3", testValue[0], 0.8222222222222222})
}
