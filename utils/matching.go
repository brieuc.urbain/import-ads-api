package utils

import (
	"github.com/adrg/strutil"
	"github.com/adrg/strutil/metrics"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"log"
	"sort"
	"strings"
	"unicode"
)

type Similarity struct {
	Value     string
	TestValue TestValue
	Score     float64
}

type TestValue struct {
	Value string
	RefId int
}

// see : https://go.dev/blog/normalization
// decompose string into runes using NFD norm and recomposes it using NFC norm without accents/marks
func NormalizeString(str string) (string, error) {
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	normStr, _, _ := transform.String(t, str)
	return strings.ToLower(normStr), nil
}

func GetNormalizedFields(userReq string) ([]string, error) {
	var normFields []string
	for _, str := range strings.Fields(userReq) {
		normStr, err := NormalizeString(str)
		if err != nil {
			log.Printf("Error cannot normalize string %s with err %s\n", str, err)
			return []string{}, err
		}
		normFields = append(normFields, normStr)
	}
	return normFields, nil
}

func GenerateTestValues(values map[int]string) []*TestValue {
	var testValues []*TestValue
	for id, str := range values {
		testValues = append(testValues, &TestValue{str, id})
	}
	return testValues
}

func NormalizeTestValues(testValues []*TestValue) error {
	for _, val := range testValues {
		normalizedValue, err := NormalizeString(val.Value)
		if err != nil {
			log.Printf("Error cannot normalize string %s with err %s\n", val.Value, err)
			return err
		}

		val.Value = normalizedValue
	}
	return nil
}

func ComputeSimilarities(normtestValues []TestValue, normFields []string) []Similarity {
	var similarities []Similarity
	for _, normStr := range normFields {
		for _, testValue := range normtestValues {
			similarity := Similarity{normStr, testValue, 0}
			similarity.Score = strutil.Similarity(normStr, testValue.Value, metrics.NewJaroWinkler())
			similarities = append(similarities, similarity)
		}
	}
	return similarities
}

func FindBestMatch(normTestValues []TestValue, normFields []string) Similarity {
	similarities := ComputeSimilarities(normTestValues, normFields)

	sort.Slice(similarities[:], func(i, j int) bool {
		return similarities[i].Score > similarities[j].Score
	})

	return similarities[0]
}
